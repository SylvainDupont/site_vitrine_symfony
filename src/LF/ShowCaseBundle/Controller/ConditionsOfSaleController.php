<?php

namespace LF\ShowCaseBundle\Controller;

use LF\ShowCaseBundle\Entity\ConditionsOfSale;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Conditionsofsale controller.
 *
 * @Route("conditionsofsale")
 */
class ConditionsOfSaleController extends Controller
{
    /**
     * Lists all conditionsOfSale entities.
     *
     * @Route("/admin", name="conditionsofsale_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $conditionsOfSales = $em->getRepository('LFShowCaseBundle:ConditionsOfSale')->findAll();

        return $this->render('@LFShowCaseBundle/showcase/conditionsofsale/index.html.twig', array(
            'conditionsOfSales' => $conditionsOfSales,
        ));
    }

    /**
     * Creates a new conditionsOfSale entity.
     *
     * @Route("/admin/new", name="conditionsofsale_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $conditionsOfSale = new Conditionsofsale();
        $form = $this->createForm('LF\ShowCaseBundle\Form\ConditionsOfSaleType', $conditionsOfSale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($conditionsOfSale);
            $em->flush();

            return $this->redirectToRoute('conditionsofsale_show', array('id' => $conditionsOfSale->getId()));
        }

        return $this->render('@LFShowCase/showcase/conditionsofsale/new.html.twig', array(
            'conditionsOfSale' => $conditionsOfSale,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a conditionsOfSale entity.
     *
     * @Route("/", name="conditionsofsale_show")
     * @Method("GET")
     */
    public function showAction()
    {
        $em = $this->getDoctrine()->getManager();
        $conditionsOfSale = $em->getRepository('LFShowCaseBundle:ConditionsOfSale')->findOneBy([
            'id' => 1
        ]);

        return $this->render('@LFShowCase/showcase/conditionsofsale/show.html.twig', array(
            'conditionsOfSale' => $conditionsOfSale,
           
        ));
    }

    /**
     * Displays a form to edit an existing conditionsOfSale entity.
     *
     * @Route("/admin/{id}/edit", name="conditionsofsale_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ConditionsOfSale $conditionsOfSale)
    {
        $deleteForm = $this->createDeleteForm($conditionsOfSale);
        $editForm = $this->createForm('LF\ShowCaseBundle\Form\ConditionsOfSaleType', $conditionsOfSale);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('@LFShowCase/showcase/conditionsofsale_edit', array('id' => $conditionsOfSale->getId()));
        }

        return $this->render('@LFShowCase/showcase/conditionsofsale/edit.html.twig', array(
            'conditionsOfSale' => $conditionsOfSale,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a conditionsOfSale entity.
     *
     * @Route("/{id}", name="conditionsofsale_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ConditionsOfSale $conditionsOfSale)
    {
        $form = $this->createDeleteForm($conditionsOfSale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($conditionsOfSale);
            $em->flush();
        }

        return $this->redirectToRoute('@LFShowCase/showcase/conditionsofsale_index');
    }

    /**
     * Creates a form to delete a conditionsOfSale entity.
     *
     * @param ConditionsOfSale $conditionsOfSale The conditionsOfSale entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ConditionsOfSale $conditionsOfSale)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('conditionsofsale_delete', array('id' => $conditionsOfSale->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
