<?php
namespace LF\ShowCaseBundle\Controller;

use LF\ShowCaseBundle\Entity\Author;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Author controller.
 *
 * @Route("author")
 */
class AuthorController extends Controller
{

    /**
     * Lists all author entities.
     *
     * @Route("admin/", name="author_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $authors = $em->getRepository('LFShowCaseBundle:Author')->findAll();
        
        return $this->render('@LFShowCase/showcase/author/index.html.twig', array(
            'authors' => $authors
        ));
    }

    /**
     * Creates a new author entity.
     *
     * @Route("admin/new", name="author_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $author = new Author();
        $form = $this->createForm('LF\ShowCaseBundle\Form\AuthorType', $author);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($author);
            $em->flush();
            
            return $this->redirectToRoute('author_show', array(
                'id' => $author->getId()
            ));
        }
        
        return $this->render('@LFShowCase/showcase/author/new.html.twig', array(
            'author' => $author,
            'form' => $form->createView()
        ));
    }

    /**
     * Finds and displays a author entity.
     *
     * @Route("/", name="author_show")
     * @Method("GET")
     */
    public function showAction()
    {
        $em = $this->getDoctrine()->getManager();
        $author = $em->getRepository('LFShowCaseBundle:Author')->findOneBy(['id'=> 1]);
        
        return $this->render('@LFShowCase/showcase/author/show.html.twig', array(
            'author' => $author,
        ));
    }

    /**
     * Displays a form to edit an existing author entity.
     *
     * @Route("admin/{id}/edit", name="author_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Author $author)
    {
        $deleteForm = $this->createDeleteForm($author);
        $editForm = $this->createForm('LF\ShowCaseBundle\Form\AuthorType', $author);
        $editForm->handleRequest($request);
        
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()
                ->getManager()
                ->flush();
            
            return $this->redirectToRoute('author_edit', array(
                'id' => $author->getId()
            ));
        }
        
        return $this->render('@LFShowCase/showcase/author/edit.html.twig', array(
            'author' => $author,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Deletes a author entity.
     *
     * @Route("admin/{id}", name="author_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Author $author)
    {
        $form = $this->createDeleteForm($author);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($author);
            $em->flush();
        }
        
        return $this->redirectToRoute('author_index');
    }

    /**
     * Creates a form to delete a author entity.
     *
     * @param Author $author
     *            The author entity
     *            
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Author $author)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('author_delete', array(
            'id' => $author->getId()
        )))
            ->setMethod('DELETE')
            ->getForm();
    }
}
