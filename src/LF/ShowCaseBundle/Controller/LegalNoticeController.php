<?php
namespace LF\ShowCaseBundle\Controller;

use LF\ShowCaseBundle\Entity\LegalNotice;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Legalnotice controller.
 *
 * @Route("legalnotice")
 */
class LegalNoticeController extends Controller
{

    /**
     * Lists all legalNotice entities.
     *
     * @Route("/admin", name="legalnotice_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $legalNotices = $em->getRepository('LFShowCaseBundle:LegalNotice')->findAll();
        
        return $this->render('@LFShowCaseBundle/showcase/legalnotice/index.html.twig', array(
            'legalNotices' => $legalNotices
        ));
    }

    /**
     * Creates a new legalNotice entity.
     *
     * @Route("/admin/new", name="legalnotice_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $legalNotice = new LegalNotice();
        $form = $this->createForm('LF\ShowCaseBundle\Form\LegalNoticeType', $legalNotice);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($legalNotice);
            $em->flush();
            
            return $this->redirectToRoute('legalnotice_show', array(
                'id' => $legalNotice->getId()
            ));
        }
        
        return $this->render('@LFShowCase/showcase/legalnotice/new.html.twig', array(
            'legalNotice' => $legalNotice,
            'form' => $form->createView()
        ));
    }

    /**
     * Finds and displays a legalNotice entity.
     *
     * @Route("/", name="legalnotice_show")
     * @Method("GET")
     */
    public function showAction()
    {
        $em = $this->getDoctrine()->getManager();
        $legalNotice = $em->getRepository('LFShowCaseBundle:LegalNotice')->findOneBy([
            'id' => 1
        ]);
        
        return $this->render('@LFShowCase/showcase/legalnotice/show.html.twig', array(
            'legalNotice' => $legalNotice
        ));
    }

    /**
     * Displays a form to edit an existing legalNotice entity.
     *
     * @Route("/admin/{id}/edit", name="legalnotice_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, LegalNotice $legalNotice)
    {
        $deleteForm = $this->createDeleteForm($legalNotice);
        $editForm = $this->createForm('LF\ShowCaseBundle\Form\LegalNoticeType', $legalNotice);
        $editForm->handleRequest($request);
        
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()
                ->getManager()
                ->flush();
            
            return $this->redirectToRoute('@LFShowCase/showcase/legalnotice_edit', array(
                'id' => $legalNotice->getId()
            ));
        }
        
        return $this->render('@LFShowCase/showcase/legalnotice/edit.html.twig', array(
            'legalNotice' => $legalNotice,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Deletes a legalNotice entity.
     *
     * @Route("/admin/{id}", name="legalnotice_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, LegalNotice $legalNotice)
    {
        $form = $this->createDeleteForm($legalNotice);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($legalNotice);
            $em->flush();
        }
        
        return $this->redirectToRoute('@LFShowCase/showcase/legalnotice_index');
    }

    /**
     * Creates a form to delete a legalNotice entity.
     *
     * @param LegalNotice $legalNotice
     *            The legalNotice entity
     *            
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(LegalNotice $legalNotice)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('legalnotice_delete', array(
            'id' => $legalNotice->getId()
        )))
            ->setMethod('DELETE')
            ->getForm();
    }
}
