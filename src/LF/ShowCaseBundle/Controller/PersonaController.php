<?php
namespace LF\ShowCaseBundle\Controller;

use LF\ShowCaseBundle\Entity\Persona;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use LF\ShowCaseBundle\Entity\Software;

/**
 * Persona controller.
 *
 * @Route("persona")
 */
class PersonaController extends Controller
{

    /**
     * Lists all persona entities.
     *
     * @Route("admin/", name="persona_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $personas = $em->getRepository('LFShowCaseBundle:Persona')->findAll();
        
        return $this->render('@LFShowCase/showcase/persona/index.html.twig', array(
            'personas' => $personas
        ));
    }

    /**
     * Creates a new persona entity.
     *
     * @Route("admin/new", name="persona_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $persona = new Persona();
        $form = $this->createForm('LF\ShowCaseBundle\Form\PersonaType', $persona);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($persona);
            $em->flush();
            
            return $this->redirectToRoute('persona_show', array(
                'id' => $persona->getId()
            ));
        }
        
        return $this->render('@LFShowCase/showcase/persona/new.html.twig', array(
            'persona' => $persona,
            'form' => $form->createView()
        ));
    }

    /**
     * Finds and displays a persona entity.
     *
     * @Route("/{id}", name="persona_show")
     * @Method("GET")
     */
    public function showAction(Persona $persona)
    {
        $em = $this->getDoctrine()->getManager();
        
        $softwares = $em->getRepository('LFShowCaseBundle:Software')->findAll();
        $deleteForm = $this->createDeleteForm($persona);
        return $this->render('@LFShowCase/showcase/persona/show.html.twig', array(
            'persona' => $persona,
            'softwares' => $softwares,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Displays a form to edit an existing persona entity.
     *
     * @Route("admin/{id}/edit", name="persona_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Persona $persona)
    {
        $deleteForm = $this->createDeleteForm($persona);
        $editForm = $this->createForm('LF\ShowCaseBundle\Form\PersonaType', $persona);
        $editForm->handleRequest($request);
        
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()
                ->getManager()
                ->flush();
            
            return $this->redirectToRoute('persona_edit', array(
                'id' => $persona->getId()
            ));
        }
        
        return $this->render('@LFShowCase/showcase/persona/edit.html.twig', array(
            'persona' => $persona,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Deletes a persona entity.
     *
     * @Route("admin/{id}", name="persona_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Persona $persona)
    {
        $form = $this->createDeleteForm($persona);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($persona);
            $em->flush();
        }
        
        return $this->redirectToRoute('persona_index');
    }

    /**
     * Creates a form to delete a persona entity.
     *
     * @param Persona $persona
     *            The persona entity
     *            
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Persona $persona)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('persona_delete', array(
            'id' => $persona->getId()
        )))
            ->setMethod('DELETE')
            ->getForm();
    }
}
