<?php

namespace LF\ShowCaseBundle\Controller;

use LF\ShowCaseBundle\Entity\Software;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Software controller.
 *
 * @Route("software")
 */
class SoftwareController extends Controller
{
    public function embedForDropdownAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $softwares = $em->getRepository('LFShowCaseBundle:Software')->findAll();
        
        return $this->render('@LFShowCase/showcase/software/dropdown.html.twig', array(
            'softwares' => $softwares,
        ));
    }
    
    
    /**
     * Lists all software entities.
     *
     * @Route("/admin", name="software_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $softwares = $em->getRepository('LFShowCaseBundle:Software')->findAll();

        return $this->render('@LFShowCase/showcase/software/index.html.twig', array(
            'softwares' => $softwares,
        ));
    }

    /**
     * Creates a new software entity.
     *
     * @Route("/admin/new", name="software_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $software = new Software();
        $form = $this->createForm('LF\ShowCaseBundle\Form\SoftwareType', $software);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($software);
            $em->flush();

            return $this->redirectToRoute('software_show', array('id' => $software->getId()));
        }

        return $this->render('@LFShowCase/showcase/software/new.html.twig', array(
            'software' => $software,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a software entity.
     *
     * @Route("/{id}", name="software_show")
     * @Method("GET")
     */
    public function showAction(Software $software)
    {
        $em = $this->getDoctrine()->getManager();
        $softwares = $em->getRepository('LFShowCaseBundle:Software')->findAll();
        $personas = $em->getRepository('LFShowCaseBundle:Persona')->findAll();

        return $this->render('@LFShowCase/showcase/software/show.html.twig', array(
            'software' => $software,
            'softwares' => $softwares,
            'personas' => $personas,
        ));
    }

    /**
     * Displays a form to edit an existing software entity.
     *
     * @Route("/admin/{id}/edit", name="software_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Software $software)
    {
        $deleteForm = $this->createDeleteForm($software);
        $editForm = $this->createForm('LF\ShowCaseBundle\Form\SoftwareType', $software);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('software_edit', array('id' => $software->getId()));
        }

        return $this->render('@LFShowCase/showcase/software/edit.html.twig', array(
            'software' => $software,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a software entity.
     *
     * @Route("/admin/{id}", name="software_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Software $software)
    {
        $form = $this->createDeleteForm($software);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($software);
            $em->flush();
        }

        return $this->redirectToRoute('software_index');
    }

    /**
     * Creates a form to delete a software entity.
     *
     * @param Software $software The software entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Software $software)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('software_delete', array('id' => $software->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
