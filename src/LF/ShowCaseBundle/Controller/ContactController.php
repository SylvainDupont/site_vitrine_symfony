<?php
namespace LF\ShowCaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use LF\ShowCaseBundle\Service\SwiftMailer\MailingService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use LF\ShowCaseBundle\Form\ContactType;

/**
 *
 * @Route("contact")
 *
 */
class ContactController extends Controller
{

    /**
     *
     * @Route("/", name="form")
     * @Method({"GET", "POST"})
     */
    public function contactAction(Request $request)
    {
        $form = $this->createForm(ContactType::class);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            
            $from = $data['email'];
            $subject = "Vous avez un nouveau message du site vitrine";
            $to = $this->getParameter('mailer_to');
            $body = $this->renderView('Emails/contact_form.html.twig', [
                'name' => $data['name'],
                'compagny' => $data['compagny'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'message' => $data['message']
            ]);
            
            $mailingService = $this->get(MailingService::class);
            $mailingService->sendMail($from, $to, $subject, $body);
            
            $this->addFlash('notice', 'Votre mail a bien été envoyé');
            
            return $this->redirectToRoute('contact');
        }
        return $this->render('@LFShowCase/showcase/contact_form.html.twig', [
            'form' => $form->createView()
        ]);
    }
}

