<?php

namespace LF\ShowCaseBundle\Controller;

use LF\ShowCaseBundle\Entity\Blog;
use LF\ShowCaseBundle\Entity\Comment;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Blog controller.
 *
 * @Route("blog")
 */
class BlogController extends Controller
{

    /**
     * @Route("/home", name="home_blog")
     * @Method("GET")
     */
    public function homeAction()
    {
        $em = $this->getDoctrine()->getManager();
        $blogs = $em->getRepository('LFShowCaseBundle:Blog')->findAll();
        return $this->render('@LFShowCase/showcase/blog/home_blog.html.twig', array(
            'blogs' => $blogs
        ));
    }

    /**
     * Lists all blog entities.
     *
     * @Route("/admin", name="blog_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $blogs = $em->getRepository('LFShowCaseBundle:Blog')->findAll();

        return $this->render('@LFShowCase/showcase/blog/index.html.twig', array(
            'blogs' => $blogs,
        ));
    }

    /**
     * Creates a new blog entity.
     *
     * @Route("/admin/new", name="blog_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $blog = new Blog();
        $form = $this->createForm('LF\ShowCaseBundle\Form\BlogType', $blog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($blog);
            $em->flush();

            return $this->redirectToRoute('blog_show', array('id' => $blog->getId()));
        }

        return $this->render('@LFShowCase/showcase/blog/new.html.twig', array(
            'blog' => $blog,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a blog entity.
     *
     * @Route("/admin/{id}", name="blog_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, Blog $blog)
    {
     $comment = new Comment();
     $form = $this->createForm('LF\ShowCaseBundle\Form\CommentType', $comment);
     $form->handleRequest($request);

     $em = $this->getDoctrine()->getManager();
     $comments = $em->getRepository('LFShowCaseBundle:Comment')->findBy(['blog' => $blog->getId()]);

     if ($form->isSubmitted() && $form->isValid()) {
        $comment->setBlog($blog);
        $em->persist($comment);
        $em->flush();

        return $this->redirectToRoute('blog_show', array('id' => $blog->getId()));
    }
    return $this->render('@LFShowCase/showcase/blog/show.html.twig', array(
        'blog' => $blog,
        'form' => $form->createView(),
        'comments' => $comments,

    ));
}

    /**
     * Displays a form to edit an existing blog entity.
     *
     * @Route("/admin/{id}/edit", name="blog_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Blog $blog)
    {
        $deleteForm = $this->createDeleteForm($blog);
        $editForm = $this->createForm('LF\ShowCaseBundle\Form\BlogType', $blog);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('blog_edit', array('id' => $blog->getId()));
        }

        return $this->render('@LFShowCaseBundle/showcase/blog/edit.html.twig', array(
            'blog' => $blog,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a blog entity.
     *
     * @Route("/admin/{id}", name="blog_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Blog $blog)
    {
        $form = $this->createDeleteForm($blog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($blog);
            $em->flush();
        }

        return $this->redirectToRoute('blog_index');
    }

    /**
     * Creates a form to delete a blog entity.
     *
     * @param Blog $blog The blog entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Blog $blog)
    {
        return $this->createFormBuilder()
        ->setAction($this->generateUrl('blog_delete', array('id' => $blog->getId())))
        ->setMethod('DELETE')
        ->getForm()
        ;
    }
}
