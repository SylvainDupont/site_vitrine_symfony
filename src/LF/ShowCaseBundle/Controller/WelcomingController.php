<?php

namespace LF\ShowCaseBundle\Controller;

use LF\ShowCaseBundle\Entity\Welcoming;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Welcoming controller.
 *
 * @Route("welcoming")
 */
class WelcomingController extends Controller
{
    /**
     * Lists all welcoming entities.
     *
     * @Route("/", name="welcoming_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $welcomings = $em->getRepository('LFShowCaseBundle:Welcoming')->findAll();

        return $this->render('@LFShowCase/showcase/welcoming/index.html.twig', array(
            'welcomings' => $welcomings,
        ));
    }

    /**
     * Creates a new welcoming entity.
     *
     * @Route("/new", name="welcoming_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $welcoming = new Welcoming();
        $form = $this->createForm('LF\ShowCaseBundle\Form\WelcomingType', $welcoming);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($welcoming);
            $em->flush();

            return $this->redirectToRoute('welcoming_show', array('id' => $welcoming->getId()));
        }

        return $this->render('@LFShowCase/showcase/welcoming/new.html.twig', array(
            'welcoming' => $welcoming,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a welcoming entity.
     *
     * @Route("/{id}", name="welcoming_show")
     * @Method("GET")
     */
    public function showAction(Welcoming $welcoming)
    {
        $deleteForm = $this->createDeleteForm($welcoming);

        return $this->render('@LFShowCase/showcase/welcoming/show.html.twig', array(
            'welcoming' => $welcoming,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing welcoming entity.
     *
     * @Route("/{id}/edit", name="welcoming_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Welcoming $welcoming)
    {
        $deleteForm = $this->createDeleteForm($welcoming);
        $editForm = $this->createForm('LF\ShowCaseBundle\Form\WelcomingType', $welcoming);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('welcoming_edit', array('id' => $welcoming->getId()));
        }

        return $this->render('@LFShowCase/showcase/welcoming/edit.html.twig', array(
            'welcoming' => $welcoming,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a welcoming entity.
     *
     * @Route("/{id}", name="welcoming_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Welcoming $welcoming)
    {
        $form = $this->createDeleteForm($welcoming);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($welcoming);
            $em->flush();
        }

        return $this->redirectToRoute('welcoming_index');
    }

    /**
     * Creates a form to delete a welcoming entity.
     *
     * @param Welcoming $welcoming The welcoming entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Welcoming $welcoming)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('welcoming_delete', array('id' => $welcoming->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
