<?php
namespace LF\ShowCaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use LF\ShowCaseBundle\Entity\Software;

class HomeController extends Controller
{

    /**
     *
     * @Route("/", name="home")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $softwares = $em->getRepository('LFShowCaseBundle:Software')->findAll();
        $personas = $em->getRepository('LFShowCaseBundle:Persona')->findAll();
        $partners = $em->getRepository('LFShowCaseBundle:Partner')->findAll();
        $welcoming = $em->getRepository('LFShowCaseBundle:Welcoming')->findOneBy([
            'id' => 1
        ]);
        $blog = $em->getRepository('LFShowCaseBundle:Blog')->findAll();
        ;
        return $this->render('@LFShowCase/showcase/home.html.twig', [
            'softwares' => $softwares,
            'personas' => $personas,
            'welcoming' => $welcoming,
            'partners' => $partners,
            'blog' => $blog,
        ]);
    }
}
