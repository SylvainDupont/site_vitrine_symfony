<?php
namespace LF\ShowCaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Persona
 *
 * @ORM\Table(name="persona")
 * @ORM\Entity(repositoryClass="LF\ShowCaseBundle\Repository\PersonaRepository")
 * @Vich\Uploadable
 */
class Persona
{

    /**
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=255)
     */
    private $role;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="descriptionContact", type="text", nullable=true)
     */
    private $descriptionContact;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="descriptionRegistration", type="text", nullable=true)
     */
    private $descriptionRegistration;

    /**
     *
     * @var array
     *
     */
    private $documents;

    /**
     *
     * @var bool
     *
     * @ORM\Column(name="contactButton", type="boolean")
     */
    private $contactButton;

    /**
     *
     * @var bool
     *
     * @ORM\Column(name="registrationButton", type="boolean")
     */
    private $registrationButton;

    /**
     *
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $personaName;

    /**
     *
     * @Vich\UploadableField(mapping="persona_image", fileNameProperty="personaName")
     *
     * @var File
     */
    private $personaFile;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $registrationName;

    /**
     *
     * @Vich\UploadableField(mapping="registration_image", fileNameProperty="registrationName")
     *
     * @var File
     */
    private $registrationFile;
    
    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    private $contactName;
    
    /**
     *
     * @Vich\UploadableField(mapping="contact_image", fileNameProperty="contactName")
     *
     * @var File
     */
    private $contactFile;

    /**
     *
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     *
     *
     * /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Persona
     */
    public function setRole($role)
    {
        $this->role = $role;
        
        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set documents
     *
     * @param array $documents
     *
     * @return Persona
     */
    public function setDocuments($documents)
    {
        $this->documents = $documents;
        
        return $this;
    }

    /**
     * Get documents
     *
     * @return array
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Persona
     */
    public function setActive($active)
    {
        $this->active = $active;
        
        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set contactButton
     *
     * @param boolean $contactButton
     *
     * @return Persona
     */
    public function setContactButton($contactButton)
    {
        $this->contactButton = $contactButton;
        
        return $this;
    }

    /**
     * Get contactButton
     *
     * @return boolean
     */
    public function getContactButton()
    {
        return $this->contactButton;
    }

    /**
     * Set registrationButton
     *
     * @param boolean $registrationButton
     *
     * @return Persona
     */
    public function setRegistrationButton($registrationButton)
    {
        $this->registrationButton = $registrationButton;
        
        return $this;
    }

    /**
     * Get registrationButton
     *
     * @return boolean
     */
    public function getRegistrationButton()
    {
        return $this->registrationButton;
    }

    /**
     * Set descriptionContact
     *
     * @param string $descriptionContact
     *
     * @return Persona
     */
    public function setDescriptionContact($descriptionContact)
    {
        $this->descriptionContact = $descriptionContact;
        
        return $this;
    }

    /**
     * Get descriptionContact
     *
     * @return string
     */
    public function getDescriptionContact()
    {
        return $this->descriptionContact;
    }

    /**
     * Set descriptionRegistration
     *
     * @param string $descriptionRegistration
     *
     * @return Persona
     */
    public function setDescriptionRegistration($descriptionRegistration)
    {
        $this->descriptionRegistration = $descriptionRegistration;
        
        return $this;
    }

    /**
     * Get descriptionRegistration
     *
     * @return string
     */
    public function getDescriptionRegistration()
    {
        return $this->descriptionRegistration;
    }

    public function setPersonaFile(File $persona = null)
    {
        $this->personaFile = $persona;
        
        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($persona) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getPersonaFile()
    {
        return $this->personaFile;
    }

    /**
     * Get personaName
     *
     * @return string
     */
    public function getPersonaName()
    {
        return $this->personaName;
    }

    /**
     * Set personaName
     *
     * @param string $personaName
     *
     * @return Persona
     */
    public function setPersonaName($personaName)
    {
        $this->personaName = $personaName;
        return $this;
    }

    public function setRegistrationFile(File $persona = null)
    {
        $this->registrationFile = $persona;
        
        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($persona) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getRegistrationFile()
    {
        return $this->registrationFile;
    }

    /**
     * Get registrationName
     *
     * @return string
     */
    public function getRegistrationName()
    {
        return $this->registrationName;
    }

    /**
     * Set registrationName
     *
     * @param string $registrationName
     *
     * @return Persona
     */
    public function setRegistrationName($registrationName)
    {
        $this->registrationName = $registrationName;
        return $this;
    }
    
    public function setContactFile(File $persona = null)
    {
        $this->contactFile = $persona;
        
        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($persona) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }
    
    public function getContactFile()
    {
        return $this->contactFile;
    }
    
    /**
     * Get contactName
     *
     * @return string
     */
    public function getContactName()
    {
        return $this->contactName;
    }
    
    /**
     * Set contactName
     *
     * @param string $contactName
     *
     * @return Persona
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;
        return $this;
    }

    // /**
    // * Constructor
    // */
    // public function __construct()
    // {
    // $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
    // }
    public function __construct()
    {
        $this->active = true;
    }
    
}
