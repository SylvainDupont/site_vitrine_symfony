<?php
namespace LF\ShowCaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Author
 *
 * @ORM\Table(name="author")
 * @ORM\Entity(repositoryClass="LF\ShowCaseBundle\Repository\AuthorRepository")
 * @Vich\Uploadable
 */
class Author
{

    /**
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="presentation", type="text")
     */
    private $presentation;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="textOne", type="text")
     */
    private $textOne;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="textTwo", type="text")
     */
    private $textTwo;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="cvOne", type="text")
     */
    private $cvOne;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="cvTwo", type="text")
     */
    private $cvTwo;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $avatarOneName;

    /**
     *
     * @Vich\UploadableField(mapping="avatar_one_image", fileNameProperty="avatarOneName")
     *
     * @var File
     */
    private $avatarOneFile;
    
    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $avatarTwoName;
    
    /**
     *
     * @Vich\UploadableField(mapping="avatar_two_image", fileNameProperty="avatarTwoName")
     *
     * @var File
     */
    private $avatarTwoFile;

    /**
     *
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set presentation
     *
     * @param string $presentation
     *
     * @return Author
     */
    public function setPresentation($presentation)
    {
        $this->presentation = $presentation;
        
        return $this;
    }

    /**
     * Get presentation
     *
     * @return string
     */
    public function getPresentation()
    {
        return $this->presentation;
    }

    /**
     * Set textOne
     *
     * @param string $textOne
     *
     * @return Author
     */
    public function setTextOne($textOne)
    {
        $this->textOne = $textOne;
        
        return $this;
    }

    /**
     * Get textOne
     *
     * @return string
     */
    public function getTextOne()
    {
        return $this->textOne;
    }

    /**
     * Set textTwo
     *
     * @param string $textTwo
     *
     * @return Author
     */
    public function setTextTwo($textTwo)
    {
        $this->textTwo = $textTwo;
        
        return $this;
    }

    /**
     * Get textTwo
     *
     * @return string
     */
    public function getTextTwo()
    {
        return $this->textTwo;
    }

    /**
     * Set cvOne
     *
     * @param string $cvOne
     *
     * @return Author
     */
    public function setCvOne($cvOne)
    {
        $this->cvOne = $cvOne;
        
        return $this;
    }

    /**
     * Get cvOne
     *
     * @return string
     */
    public function getCvOne()
    {
        return $this->cvOne;
    }

    /**
     * Set cvTwo
     *
     * @param string $cvTwo
     *
     * @return Author
     */
    public function setCvTwo($cvTwo)
    {
        $this->cvTwo = $cvTwo;
        
        return $this;
    }

    /**
     * Get cvTwo
     *
     * @return string
     */
    public function getCvTwo()
    {
        return $this->cvTwo;
    }

    public function setAvatarOneFile(File $persona = null)
    {
        $this->avatarOneFile = $persona;
        
        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($persona) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getAvatarOneFile()
    {
        return $this->avatarOneFile;
    }

    /**
     * Get avatarOneName
     *
     * @return string
     */
    public function getAvatarOneName()
    {
        return $this->avatarOneName;
    }

    /**
     * Set avatarOneName
     *
     * @param string $avatarOneName
     *
     * @return Author
     */
    public function setAvatarOneName($avatarOneName)
    {
        $this->avatarOneName = $avatarOneName;
        return $this;
    }
    
    
    public function setAvatarTwoFile(File $persona = null)
    {
        $this->avatarTwoFile = $persona;
        
        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($persona) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }
    
    public function getAvatarTwoFile()
    {
        return $this->avatarTwoFile;
    }
    
    /**
     * Get avatarTwoName
     *
     * @return string
     */
    public function getAvatarTwoName()
    {
        return $this->avatarTwoName;
    }
    
    /**
     * Set avatarTwoName
     *
     * @param string $avatarTwoName
     *
     * @return Author
     */
    public function setAvatarTwoName($avatarTwoName)
    {
        $this->avatarTwoName = $avatarTwoName;
        return $this;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Author
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
