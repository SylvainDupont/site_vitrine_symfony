<?php

namespace LF\ShowCaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Course
 *
 * @ORM\Table(name="course")
 * @ORM\Entity(repositoryClass="LF\ShowCaseBundle\Repository\CourseRepository")
 */
class Course
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="doc", type="string", length=255, nullable=true)
     */
    private $doc;

    /**
     * @var bool
     *
     * @ORM\Column(name="certification", type="boolean")
     */
    private $certification;
    
    
    /**
     * 
     * @ORM\ManyToOne(targetEntity="Software", inversedBy="courses")
     */
    private $software;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Course
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set doc
     *
     * @param string $doc
     *
     * @return Course
     */
    public function setDoc($doc)
    {
        $this->doc = $doc;

        return $this;
    }

    /**
     * Get doc
     *
     * @return string
     */
    public function getDoc()
    {
        return $this->doc;
    }

    /**
     * Set certification
     *
     * @param boolean $certification
     *
     * @return Course
     */
    public function setCertification($certification)
    {
        $this->certification = $certification;

        return $this;
    }

    /**
     * Get certification
     *
     * @return bool
     */
    public function getCertification()
    {
        return $this->certification;
    }
    
    /**
     * Set software
     *
     * @param string $software
     *
     * @return Course
     */
    public function setSoftware($software)
    {
        $this->software = $software;
        
        return $this;
    }
    
    /**
     * Get software
     *
     * @return string
     */
    public function getSoftware()
    {
        return $this->software;
    }
    
    public function __toString()
    {
        return $this->getName();
    }
}

