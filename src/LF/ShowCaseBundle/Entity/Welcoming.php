<?php

namespace LF\ShowCaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Welcoming
 *
 * @ORM\Table(name="welcoming")
 * @ORM\Entity(repositoryClass="LF\ShowCaseBundle\Repository\WelcomingRepository")
 */
class Welcoming
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="presentation", type="text")
     */
    private $presentation;

    /**
     * @var string
     *
     * @ORM\Column(name="titleSoftware", type="string", length=255)
     */
    private $titleSoftware;

    /**
     * @var string
     *
     * @ORM\Column(name="titlePersona", type="string", length=255)
     */
    private $titlePersona;

/**
* @var string
* @ORM\Column(name="titlePartner", type="string", length=255)
*
*/
    private $titlePartner;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set presentation
     *
     * @param string $presentation
     *
     * @return Welcoming
     */
    public function setPresentation($presentation)
    {
        $this->presentation = $presentation;

        return $this;
    }

    /**
     * Get presentation
     *
     * @return string
     */
    public function getPresentation()
    {
        return $this->presentation;
    }

    /**
     * Set titleSoftware
     *
     * @param string $titleSoftware
     *
     * @return Welcoming
     */
    public function setTitleSoftware($titleSoftware)
    {
        $this->titleSoftware = $titleSoftware;

        return $this;
    }

    /**
     * Get titleSoftware
     *
     * @return string
     */
    public function getTitleSoftware()
    {
        return $this->titleSoftware;
    }

    /**
     * Set titlePersona
     *
     * @param string $titlePersona
     *
     * @return Welcoming
     */
    public function setTitlePersona($titlePersona)
    {
        $this->titlePersona = $titlePersona;

        return $this;
    }

    /**
     * Get titlePersona
     *
     * @return string
     */
    public function getTitlePersona()
    {
        return $this->titlePersona;
    }

     /**
     * Set titlePartner
     *
     * @param string $titlePartner
     *
     * @return Welcoming
     */
    public function setTitlePartner($titlePartner)
    {
        $this->titlePartner = $titlePartner;

        return $this;
    }

    /**
     * Get titlePartner
     *
     * @return string
     */
    public function getTitlePartner()
    {
        return $this->titlePartner;
    }
}

