<?php

namespace LF\ShowCaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConditionsOfSale
 *
 * @ORM\Table(name="conditions_of_sale")
 * @ORM\Entity(repositoryClass="LF\ShowCaseBundle\Repository\ConditionsOfSaleRepository")
 */
class ConditionsOfSale
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return ConditionsOfSale
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}

