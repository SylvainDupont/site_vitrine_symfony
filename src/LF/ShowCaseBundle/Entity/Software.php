<?php

namespace LF\ShowCaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Software
 *
 * @ORM\Table(name="software")
 * @ORM\Entity(repositoryClass="LF\ShowCaseBundle\Repository\SoftwareRepository")
 * @Vich\Uploadable
 */
class Software
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * @var string
     *
     * @ORM\Column(name="descriptive", type="text")
     */
    private $descriptive;

    /**
     * 
     * @ORM\OneToMany(targetEntity="Course", mappedBy="software")
     */
    private $courses;

  
    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $softwareName;
    
    /**
     *
     * @Vich\UploadableField(mapping="software_image", fileNameProperty="softwareName")
     *
     * @var File
     */
    private $softwareFile;
    
    /**
     *
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;
    

public function __construct()
{
    $this->courses = new ArrayCollection();
}
    
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Software
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set descriptive
     *
     * @param string $descriptive
     *
     * @return Software
     */
    public function setDescriptive($descriptive)
    {
        $this->descriptive = $descriptive;

        return $this;
    }

    /**
     * Get descriptive
     *
     * @return string
     */
    public function getDescriptive()
    {
        return $this->descriptive;
    }

    /**
     * Set courses
     *
     * @param string $courses
     *
     * @return Software
     */
    public function setCourses($courses)
    {
        $this->courses = $courses;

        return $this;
    }

    /**
     * Get courses
     *
     * @return string
     */
    public function getCourses()
    {
        return $this->courses;
    }

  
    
    /*
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Software
     */
    public function getSoftwareFile()
    {
        return $this->softwareFile;
    }
    
    public function setSoftwareFile(File $software = null)
    {
        $this->softwareFile = $software;
        if ($software) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
        return $this;
    }
    
    /**
     * Get softwareName
     *
     * @return string
     */
    public function getSoftwareName()
    {
        return $this->softwareName;
    }
    
    /**
     * Set softwareName
     *
     * @param string $softwareName
     *
     * @return Software
     */
    public function setSoftwareName($softwareName)
    {
        $this->softwareName = $softwareName;
        return $this;
    }
    
    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Software
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
    
    public function addCourse(Course $course)
    {
        if (!$this->courses->contains($course)) {
            $this->courses[]= $course;
            $course->setSoftware($this);
        }
        return $this;
    }
    
    public function removeCourse(Course $scourse)
    {
        $this->courses->removeElement($course);
        $course->setSoftware();
    }
    
    public function getCourse()
    {
        return $this->courses;
    }
    
}

