<?php
namespace LF\ShowCaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PersonaType extends AbstractType
{

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('role', TextType::class, [
            'label' => 'Nom'
        ])
            ->add('descriptionContact', CKEditorType::class, [
            'label' => 'Créer un nouveau persona',
        ])
            ->add('personaFile', VichImageType::class, array(
            'label' => 'Votre image'
        ))
            ->add('contactButton', CheckboxType::class, [
            'label' => 'Bouton Contact',
            'required' => false
        ])
            ->add('descriptionRegistration', CKEditorType::class, [
            'label' => 'Créer un nouveau persona',
        ])
            ->add('registrationFile', VichImageType::class, array(
            'label' => 'Votre image'
        ))
            ->add('contactFile', VichImageType::class, array(
            'label' => 'Votre image'
        ))
            ->add('contactButton', CheckboxType::class, [
            'label' => 'Bouton Contact',
            'required' => false
        ])
            ->add('registrationButton', CheckboxType::class, [
            'label' => 'Bouton Inscription',
            'required' => false
        ])
            ->add('active', CheckboxType::class, [
            'label' => 'Annonce active',
            'required' => false
        ]);
    }

    /**
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LF\ShowCaseBundle\Entity\Persona'
        ));
    }

    /**
     *
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'lf_showcasebundle_persona';
    }
}
