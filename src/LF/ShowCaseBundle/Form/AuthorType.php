<?php
namespace LF\ShowCaseBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class AuthorType extends AbstractType
{

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('presentation')
            ->add('textOne')
            ->add('textTwo')
            ->add('cvOne')
            ->add('cvTwo')
            ->
        add('avatarOneFile', VichImageType::class, array(
            'label' => 'Votre image'
        ))
            ->add('avatarTwoFile', VichImageType::class, array(
            'label' => 'Votre image'
        ));
    }

    /**
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LF\ShowCaseBundle\Entity\Author'
        ));
    }

    /**
     *
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'lf_showcasebundle_author';
    }
}
