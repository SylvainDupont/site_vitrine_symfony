<?php
namespace LF\ShowCaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;

class ContactType extends AbstractType
{

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => "Nom et prénom"
        ])
            ->add('email', EmailType::class, [
            'label' => "Email"
        ])
            ->add('phone', TelType::class, [
            'label' => "Téléphone"
        ])
            ->add('compagny', TextType::class, [
            'label' => "Société"
        ])
            ->add('message', TextareaType::class, [
            'label' => "Votre message",
            'attr' => array(
                'rows' => 10
            )
        ])
        ->getForm();
        
    }

    /**
     *
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null
        ));
    }

    /**
     *
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'contact';
    }
}

