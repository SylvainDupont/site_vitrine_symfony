<?php
namespace LF\ShowCaseBundle\Service\SwiftMailer;

class MailingService
{

    private $mailer;

    /**
     * MailingService constructor.
     * 
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     *
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param string $body
     *
     */
    public function sendMail(string $from, string $to, string $subject, string $body)
    {
        $message = (new \Swift_Message())->setFrom($from)
            ->setTo($to)
            ->setSubject($subject)
            ->setBody($body, 'text/html');
        $this->mailer->send($message);
    }
}
    
